module.exports = {
    apps: [{
      name: "Stream-WebRTC",
      script: "app.js"
    }],
    deploy: {
      // "production" is the environment name
      production: {
        // SSH key path, default to $HOME/.ssh
        //key: "/path/to/some.pem",
        // SSH user
        user: "chuclucillo",
        // SSH host
        host: ["ovh1.dedomingo.net"],
        // SSH options with no command-line flag, see 'man ssh'
        // can be either a single string or an array of strings
        ssh_options: [
            "StrictHostKeyChecking=no",
            "Port=8282"
        ],
        // GIT remote/branch
        ref: "origin/master",
        // GIT remote
        repo: "git clone https://Chuclucillo@bitbucket.org/chucluteam/stream-webrtc-server.git",
        // path in the server
        path: "/srv/node/streamingwebrtc",
        // Pre-setup command or path to a script on your local machine
        //pre-setup: "apt-get install git ; ls -la",

        // Post-setup commands or path to a script on the host machine
        // eg: placing configurations in the shared dir etc
        //post-setup: "ls -la",

        // pre-deploy action
        pre_deploy_local: "echo 'This is a local executed command'",
        // post-deploy action
        post_deploy: "npm install",
      },
    }
  }
module.exports = {
  apps : [{
    name: 'StreamingWebRTC',
    script: 'app.js',

    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    args: '',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }],

  deploy : {
    production : {
      user : 'chuclucillo',
      host : 'ovh1.dedomingo.net',
      port : '8282',
      ref  : 'origin/master',
      repo : 'https://Chuclucillo@bitbucket.org/chucluteam/stream-webrtc-server.git',
      path : '/srv/node/streamingwebrtc',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
